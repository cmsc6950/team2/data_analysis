.PHONY: all requirements dataInsight bokehPlot download help

all :  help venv runProject

PYTHON_INTERPRETER = python

venv:
				pip install --user pipenv
				pipenv install

dataInsight : download
				pipenv run python data_insight.py

bokehPlot : download
				pipenv run python bokeh_plot.py

runProject :
				pipenv run python run_init.py

download :
				pipenv run python download.py


.DEFAULT: help
help:
	 @echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
	 @echo "make venv"
	 @echo "       install and create virtual environment"
	 @echo "make download"
	 @echo "       run to download Black Friday dataset from kaggle"
	 @echo "make dataInsight"
	 @echo "       run to plot matplotlib plots"
	 @echo "make bokehPlot"
	 @echo "       run to plot Bokeh plot"
	 @echo "make runProject"
	 @echo "       run complete project"
	 @echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
