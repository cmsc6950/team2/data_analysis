import os
import json
from zipfile import ZipFile
import platform

# Method to create token file
def create_token_file(kaggle_token_file):

    plat_form = platform.system()
    folder_path = os.path.expanduser('~')
    folder_path = folder_path +"/.kaggle"

    # create directory for token file
    if not os.path.exists(folder_path):
        os.mkdir(folder_path)

    if os.path.exists(folder_path):
        folder_path = folder_path +"/" + kaggle_token_file
        token = {"username":"kaushlenderk","key":"39d38a43aaf0fb250f887e943aa11e17"}
        json_token = json.dumps(token)
        f = open(folder_path,"w+")
        f.write(json_token)
        f.close()
        print("\nSuccessfully created kaggle token file........")


# Method to unzip folder
def unzip_folder(folder_path):
    with ZipFile(folder_path, 'r') as zipObj:
        zipObj.extractall("dataset/")

    print("\nZip folder extraction done........\n")

# Method to call sub methods
def download():
    try:
        # code to install kaggle
        try:

            # Create kaggle token json file
            kaggle_token_file = "kaggle.json"
            create_token_file(kaggle_token_file)

            import kaggle
            print("\nkaggle package all ready installed........\n")
        except ImportError as e:
            os.system("pip install kaggle")
            print("\nkaggle package installation is done........\n")



        # list the black-friday folder structure
        os.system("kaggle datasets list -s black-friday")

        # download the black-friday zip folder
        if not os.path.exists("dataset/black-friday.zip"):
            os.system("kaggle datasets download -d mehdidag/black-friday -p dataset/")

        # Extract all the contents of black-friday.zip folder
        unzip_folder('dataset/black-friday.zip')

    except Exception as e:
        print(e)

if __name__ == "__main__":

    try:
        # call file download process
        download()

    except Exception as e:
        print(e)
