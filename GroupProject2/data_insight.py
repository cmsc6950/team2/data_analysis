import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from mpl_toolkits.mplot3d import Axes3D
import seaborn as sns
from sklearn.preprocessing import Imputer
import download as dw
import load_dataset

# plot columns data
def plot_column_distribution(data_frame, ngraph_shown, ngraph_perrow):
    nunique = data_frame.nunique()
    data_frame = data_frame[[col for col in data_frame if nunique[col] > 1 and nunique[col] < 50]]
    nRow, nCol = data_frame.shape
    column_names = list(data_frame)
    ngraph_row = (nCol + ngraph_perrow - 1) / ngraph_perrow
    plt.figure(num = None, figsize = (6 * ngraph_perrow, 8 * ngraph_row), dpi = 300, facecolor = 'w', edgecolor = 'k')
    for i in range(min(nCol, ngraph_shown)):
        plt.subplot(ngraph_row, ngraph_perrow, i + 1)
        columnDf = data_frame.iloc[:, i]
        if (not np.issubdtype(type(columnDf.iloc[0]), np.number)):
            value_counts = columnDf.value_counts()
            value_counts.plot.bar()
        else:
            columnDf.hist()
        plt.ylabel('counts')
        plt.xticks(rotation = 90)
        plt.title(f'{column_names[i]}')
    plt.tight_layout(pad = 5.0, w_pad = 5.0, h_pad = 6.0)
    plt.show()


# plot correlation matrix
def plot_correlation_matrix(blackfriday_data):
    plt.figure(figsize=(12,5))
    sns.heatmap(
        blackfriday_data.corr(),
        annot=True,
        linewidth = 0.5
    )


# Consumers purchase behaviour
def purchase_occupation_behaviour(blackfriday_data):
    sns.jointplot(x='Occupation',y='Purchase',
              data=blackfriday_data, kind='hex', color="b",height=8
             )

    fig = plt.figure(figsize=(12,6))
    ax = fig.add_subplot(111)

    sns.boxplot(x = 'Occupation', y = 'Purchase', data = blackfriday_data, ax=ax)
    ax.set_title("Boxplot: purchase amount by occupations")
    plt.show()

# Consumer behaviour based on Age, Gender, Marital_Status,City_Category
def consumer_behaviour(blackfriday_data):
    fig, axes = plt.subplots(nrows=2, ncols=2,figsize=(18,14))

    blackfriday_data['Age_Encoded'] = blackfriday_data['Age'].map({'0-17':0,'18-25':1,
                          '26-35':2,'36-45':3,
                          '46-50':4,'51-55':5,
                          '55':6})

    ax = sns.boxplot(y='Purchase', x=blackfriday_data['Age'].sort_values(),data=blackfriday_data, ax=axes[0,0])
    ax = sns.boxplot(y='Purchase', x='Gender',data=blackfriday_data, ax=axes[0,1])
    ax = sns.boxplot(y='Purchase', x='Marital_Status',data=blackfriday_data,ax=axes[1,0])
    ax = sns.boxplot(y='Purchase', x='City_Category',data=blackfriday_data,ax=axes[1,1])

# Consumer behaviour based on Age, city, Purchase
def consumer_behaviour_type1(blackfriday_data):
    fig, axes = plt.subplots(nrows=2, ncols=2,figsize=(18,14))

    ax = sns.pointplot(y='Product_Category_1', x='City_Category', hue='Age', data=blackfriday_data, ax=axes[0,0])
    ax = sns.pointplot(y='Product_Category_2', x='City_Category', hue='Age', data=blackfriday_data, ax=axes[0,1])
    ax = sns.pointplot(y='Product_Category_3', x='City_Category', hue='Age', data=blackfriday_data, ax=axes[1,0])
    ax = sns.pointplot(y='Purchase', x='City_Category', hue='Age',data=blackfriday_data, ax=axes[1,1])


# Average Purchases made by Age group
def plot_average_purchase_by_age(blackfriday_data):

    dfData = pd.DataFrame()
    dfData = blackfriday_data[['Age','Purchase']]

    # Throw error in case of empty black firday data set
    assert dfData['Purchase'].count() != 0,"Assert error Black Friday Dataset is is empty."

    # calculate average purchase by age
    dfData['Purchase'] = dfData['Purchase'].astype(float)
    df_average = dfData.groupby(dfData['Age']).mean()

    df_average["Purchase"].plot.pie(autopct='%1.1f%%',counterclock=False, shadow=True,figsize=(9, 9))
    plt.title('Average Purchases made by Age group')
    plt.axes().set_ylabel('')
    plt.axes().set_xlabel('')
    plt.show()

def call_data_insight_method(blackfriday_data):

    # Plot to get initial insight of the data points
    plot_column_distribution(blackfriday_data, 15, 4)

    # Plot correlation matrix of black friday dataset
    plot_correlation_matrix(blackfriday_data)

    # Consumer purchase behaviour
    # joint plot
    purchase_occupation_behaviour(blackfriday_data)

    # Consumer behaviour based on Age, Gender, Marital_Status,City_Category
    consumer_behaviour(blackfriday_data)

    # Consumer behaviour based on Age, city, Purchase
    consumer_behaviour_type1(blackfriday_data)

    # Average Purchases made by Age group
    plot_average_purchase_by_age(blackfriday_data)


if __name__ == "__main__":

    try:
        # Load dataset
        new_file_name = "dataset/Clean_BlackFriday.csv"

        blackfriday_data = load_dataset.load_data(new_file_name)
        blackfriday_data.dataframeName = 'Black Friday dataset'

        call_data_insight_method(blackfriday_data)
        
    except Exception as e:
        print(e)
