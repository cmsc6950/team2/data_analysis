\babel@toc {english}{}
\contentsline {section}{\numberline {1}Motivations}{3}% 
\contentsline {section}{\numberline {2}Black Friday dataset insight plot}{4}% 
\contentsline {section}{\numberline {3}Correlation matrix}{5}% 
\contentsline {section}{\numberline {4}Buyers' purchase amount distribution }{6}% 
\contentsline {section}{\numberline {5}Consumer behaviours plot}{6}% 
\contentsline {subsection}{\numberline {5.1}Consumer purchase behaviour based on their occupation}{6}% 
\contentsline {subsection}{\numberline {5.2}Box Plot: Consumer purchase habits across all occupation types}{8}% 
\contentsline {subsection}{\numberline {5.3}Box Plot: Consumer purchase behaviour across various categories}{8}% 
\contentsline {subsection}{\numberline {5.4}BAR Plot: Consumer purchase behaviour across various categories}{9}% 
\contentsline {subsection}{\numberline {5.5}Point Plots: Consumer purchase behaviour based on their age and city they live in}{11}% 
\contentsline {subsection}{\numberline {5.6}Pie Plot: Consumer average purchase behaviour based on their age categories}{12}% 
