import download as dw
import data_insight as dInsight
import bokeh_plot as bp
import load_dataset


if __name__ == "__main__":

    try:
        # Call download process
        dw.download()

        # Call method to load black friday data from files
        file_name = "dataset/BlackFriday.csv"
        blackfriday_dataframe = load_dataset.load_data(file_name)

        # Clean dataset remove/replace missing value from dataset
        new_file_name = "dataset/Clean_BlackFriday.csv"
        load_dataset.clean_dataset(blackfriday_dataframe,new_file_name)

        # Load dataset
        blackfriday_data = load_dataset.load_data(new_file_name)
        blackfriday_data.dataframeName = 'Black Friday dataset'

        # Plot to get initial insight of the data points
        dInsight.call_data_insight_method(blackfriday_data)

        # bokeh plot
        bp.call_bokeh_plot(blackfriday_data)

    except Exception as e:
        print(e)
