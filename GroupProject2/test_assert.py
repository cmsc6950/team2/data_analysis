import load_dataset
import bokeh_plot as bp
import pytest
import pandas as pd

def test_empty_dataset():

    new_file_name = "dataset/Clean_BlackFriday.csv"
    blackfriday_dataframe = load_dataset.load_data(new_file_name)

    assert blackfriday_dataframe['Purchase'].count() != 0,"Purchase column of Black Friday Dataset is empty."

def test_empty_age_field_plot():

    new_file_name = "dataset/Clean_BlackFriday.csv"
    blackfriday_dataframe = load_dataset.load_data(new_file_name)

    dfData = pd.DataFrame()
    dfData=blackfriday_dataframe[['Age','Purchase']]
    dfData['Age'] = dfData['Age'].apply(bp.map_age_field)

    assert dfData['Age'].count() != 0,"Age column of Black Friday Dataset is empty"

def test_empty_gender_field_plot():

    new_file_name = "dataset/Clean_BlackFriday.csv"
    blackfriday_dataframe = load_dataset.load_data(new_file_name)

    dfData = pd.DataFrame()
    dfData=blackfriday_dataframe[['Gender','Purchase']]
    dfData['Gender'] = dfData['Gender'].apply(bp.map_gender_field)

    assert dfData['Gender'].count() != 0,"Gender column of Black Friday Dataset is empty."

def test_empty_city_field_plot():

    new_file_name = "dataset/Clean_BlackFriday.csv"
    blackfriday_dataframe = load_dataset.load_data(new_file_name)

    dfData = pd.DataFrame()
    dfData=blackfriday_dataframe[['City_Category','Purchase']]
    dfData['City_Category'] = dfData['City_Category'].apply(bp.map_city_categories_field)

    assert dfData['City_Category'].count() != 0,"City_Category column of Black Friday Dataset is empty"