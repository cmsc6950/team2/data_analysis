import pandas as pd

# Load black friday data set
def load_data(file_name):
    blackfriday_data = pd.read_csv(file_name)
    return blackfriday_data

# remove missing value or replace value NaN value with 0
def clean_dataset(blackfriday_data,file_name):

    # Replace NaN value with 0
    blackfriday_data = blackfriday_data.fillna(0)

    # drop below columns
    cols = ['User_ID','Product_ID']
    blackfriday_data.drop(cols, inplace = True, axis =1)

    # remove + from data points
    blackfriday_data['Age']=(blackfriday_data['Age'].str.strip('+'))
    blackfriday_data['Stay_In_Current_City_Years']=(blackfriday_data['Stay_In_Current_City_Years'].str.strip('+').astype('float'))


    blackfriday_data.Product_Category_2 = blackfriday_data.Product_Category_2.astype(int)
    blackfriday_data.Product_Category_3 = blackfriday_data.Product_Category_3.astype(int)

    # write dataframe to files
    blackfriday_data.to_csv(file_name, index=False)

if __name__ == "__main__":

    try:

        # Call method to load black friday data from files
        file_name = "dataset/BlackFriday.csv"
        blackfriday_dataframe = load_data(file_name)

        # Clean dataset remove/replace missing value from dataset
        new_file_name = "dataset/Clean_BlackFriday.csv"
        clean_dataset(blackfriday_dataframe,new_file_name)

    except Exception as e:
        print(e)
