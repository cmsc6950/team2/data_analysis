import pandas as pd
from math import pi
from bokeh.io import output_file, save,show
from bokeh.palettes import  viridis, Category10
from bokeh.plotting import figure
from bokeh.transform import cumsum
import holoviews as hv
from holoviews import opts, Palette
from bokeh.palettes import Category20c
from bokeh.models import HoverTool
import numpy as np
import load_dataset


def map_age_field(age):
    if age == '0-17':
        return 0
    elif age == '18-25':
        return 1
    elif age == '26-35':
        return 2
    elif age == '36-45':
        return 3
    elif age == '46-50':
        return 4
    elif age == '51-55':
        return 5
    else:
        return 6


def plot_bar_age_vs_purchase(blackfriday_data):

    dfData = pd.DataFrame()
    dfData=blackfriday_data[['Age','Purchase']]
    dfData['Age'] = dfData['Age'].apply(map_age_field)

    # calculate average purchase by age
    dfData['Purchase'] = dfData['Purchase'].astype(float)
    df_average = dfData.groupby(dfData['Age']).count().reset_index()

    TOOLS = "hover,save,pan,box_zoom,reset,wheel_zoom,tap"
    p = figure(plot_height=350, title="Purchase amount by Age",tools=TOOLS,toolbar_location='above')

    p.vbar(x=df_average.Age, top=df_average.Purchase, width=0.9)

    p.y_range.start = 0
    p.x_range.range_padding = 0.1
    p.xgrid.grid_line_color = None
    p.axis.minor_tick_line_color = None
    p.outline_line_color = None
    p.xaxis.axis_label = 'Age'
    p.yaxis.axis_label = 'Purchase'
    p.select_one(HoverTool).tooltips = [
        ('Age', '@x'),
        ('Purchase', '@top'),
    ]
    output_file("BokehPlot/bar_age_vs_purchase.html", title="barchart")
    show(p)

def map_gender_field(gender):
    if gender == 'F':
        return 1
    else:
        return 0

def plot_bar_gender_vs_purchase(blackfriday_data):
    dfData = pd.DataFrame()
    dfData=blackfriday_data[['Gender','Purchase']]
    dfData['Gender'] = dfData['Gender'].apply(map_gender_field)


    # calculate average purchase by age
    dfData['Purchase'] = dfData['Purchase'].astype(float)
    df_average = dfData.groupby(dfData['Gender']).count().reset_index()


    TOOLS = "hover,save,pan,box_zoom,reset,wheel_zoom,tap"
    p = figure(plot_height=350, title="Purchase amount by Gender",tools=TOOLS,toolbar_location='above')

    p.vbar(x=df_average.Gender, top=df_average.Purchase, width=0.9)

    p.y_range.start = 0
    p.x_range.range_padding = 0.1
    p.xgrid.grid_line_color = None
    p.axis.minor_tick_line_color = None
    p.outline_line_color = None
    p.xaxis.axis_label = 'Gender'
    p.yaxis.axis_label = 'Purchase'
    p.select_one(HoverTool).tooltips = [
        ('Gender', '@x'),
        ('Purchase', '@top'),
    ]
    output_file("BokehPlot/bar_Gender_vs_purchase.html", title="barchart")
    show(p)

def plot_bar_maritalstatus_vs_purchase(blackfriday_data):
    dfData = pd.DataFrame()
    dfData=blackfriday_data[['Marital_Status','Purchase']]

    # calculate average purchase by age
    dfData['Purchase'] = dfData['Purchase'].astype(float)
    df_average = dfData.groupby(dfData['Marital_Status']).count().reset_index()


    TOOLS = "hover,save,pan,box_zoom,reset,wheel_zoom,tap"
    p = figure(plot_height=350, title="Purchase amount by Marital_Status",tools=TOOLS,toolbar_location='above')

    p.vbar(x=df_average.Marital_Status, top=df_average.Purchase, width=0.9)

    p.y_range.start = 0
    p.x_range.range_padding = 0.1
    p.xgrid.grid_line_color = None
    p.axis.minor_tick_line_color = None
    p.outline_line_color = None
    p.xaxis.axis_label = 'Marital Status'
    p.yaxis.axis_label = 'Purchase'
    p.select_one(HoverTool).tooltips = [
        ('Marital Status', '@x'),
        ('Purchase', '@top'),
    ]
    output_file("BokehPlot/bar_Marital_Status_vs_purchase.html", title="barchart")
    show(p)

def map_city_categories_field(city_category):
    if city_category == 'A':
        return 2
    elif city_category == 'B':
        return 1
    else:
        return 0


def plot_bar_city_category_vs_purchase(blackfriday_data):
    dfData = pd.DataFrame()
    dfData=blackfriday_data[['City_Category','Purchase']]
    dfData['City_Category'] = dfData['City_Category'].apply(map_city_categories_field)

    # calculate average purchase by age
    dfData['Purchase'] = dfData['Purchase'].astype(float)
    df_average = dfData.groupby(dfData['City_Category']).count().reset_index()


    TOOLS = "hover,save,pan,box_zoom,reset,wheel_zoom,tap"
    p = figure(plot_height=350, title="Purchase amount by City Category",tools=TOOLS,toolbar_location='above')

    p.vbar(x=df_average.City_Category, top=df_average.Purchase, width=0.9)

    p.y_range.start = 0
    p.x_range.range_padding = 0.1
    p.xgrid.grid_line_color = None
    p.axis.minor_tick_line_color = None
    p.outline_line_color = None
    p.xaxis.axis_label = 'City Category'
    p.yaxis.axis_label = 'Purchase'
    p.select_one(HoverTool).tooltips = [
        ('City Category', '@x'),
        ('Purchase', '@top'),
    ]
    output_file("BokehPlot/bar_City_Category_vs_purchase.html", title="barchart")
    show(p)

def plot_average_purchase_by_age(blackfriday_data):

    output_file("pie.html")

    dfData = pd.DataFrame()
    dfData=blackfriday_data[['Age','Purchase']]

    # calculate average purchase by age
    dfData['Purchase'] = dfData['Purchase'].astype(float)
    df_average = dfData.groupby(dfData['Age']).mean().reset_index()

    df_average['angle'] = df_average['Purchase']/df_average['Purchase'].sum() * 2*pi

    df_average['color'] = Category20c[len(df_average['Purchase'])]

    p = figure(plot_height=350, title="Average purchases made by Age groups", toolbar_location=None,
               tools="hover", tooltips="@Age: @Purchase", x_range=(-0.5, 1.0))

    p.wedge(x=0, y=1, radius=0.4,  start_angle=cumsum('angle', include_zero=True), end_angle=cumsum('angle'),
            line_color="white", fill_color='color', legend='Age', source=df_average)


    p.axis.axis_label=None
    p.axis.visible=False
    p.grid.grid_line_color = None

    output_file("BokehPlot/pie.html", title="barchart")
    show(p)


# main method to call sub plot method
def call_bokeh_plot(blackfriday_data):

    plot_bar_age_vs_purchase(blackfriday_data)

    plot_bar_gender_vs_purchase(blackfriday_data)

    plot_bar_maritalstatus_vs_purchase(blackfriday_data)

    plot_bar_city_category_vs_purchase(blackfriday_data)

    plot_average_purchase_by_age(blackfriday_data)


if __name__ == "__main__":

    try:

        # Load dataset
        new_file_name = "dataset/Clean_BlackFriday.csv"

        blackfriday_data = load_dataset.load_data(new_file_name)
        blackfriday_data.dataframeName = 'Black Friday dataset'

        call_bokeh_plot(blackfriday_data)
        
    except Exception as e:
        print(e)
