# Black Friday

## A study of sales through consumer behaviours

### Description: 
We conducted an analysis of Black Friday data sets provided by Kaggle to understand customers’ purchase behaviour based 
on their gender, age, occupation and the region they live in.

### AIM: 
The aim of our team was to analyse Black Friday datasets provided by Kaggle and draw various insights from them. We wanted to understand customers’
purchase behaviour based on certain parameters such as their gender, age, occupation and the region they live in, to use these ﬁndings to improve 
advertisement campaigns and target the right population and cities.

Download Path: [kaggle-BlackFriday](https://www.kaggle.com/mehdidag/black-friday#BlackFriday.csv)

![](GroupProject2/Graph/Black_Friday.jpg)


## Report Links

> [Project Report](https://gitlab.com/cmsc6950/team2/data_analysis/raw/master/GroupProject2/Report/Team_2_report.pdf?inline=false)

> [Project Presentation](https://cmsc6950.gitlab.io/team2/data_analysis/GroupProject2/public/index.html) 


## Black Friday dataset insight plot

>   Plots of Black Friday dataset ﬁelds to gain an initial understanding of the source and target data ﬁelds.

![](GroupProject2/Graph/data_insight.png)


##  Correlation matrix

>    There is a positive correlation between three of the below ﬁelds with the target ﬁeld purchase. 
     An increase in value of any of the below ﬁelds is likely a result of a higher purchase from the customer.
  
>>    1. Occupation
>>    2. Stay_In_Current_City_Years
>>    3. Marital Status 
        

![](GroupProject2/Graph/correlation_matrix.png)

## Buyers’ purchase amount distribution

> The distribution of the amount spent in purchases by the buyers almost followed Gaussian distribution.

![](GroupProject2/Graph/purchase_amount_distribution.png)


## Consumer behaviours plot

### Consumer purchase behaviour based on their occupation

>   Occupation versus Purchase
    
>>  1.  Most of the purchases ranged between USD$5000 and USD$10,000.
>>  2.  The occupation type 4 registered the highest purchases, followed by occupation types 0 and 7. 
>>      Insight: From this data, we can draw insights that companies can plan to run targeted advertisements around the occupation 
>>      types 4, 0 and 7 to sell their products and increase their chances of gaining more proﬁt.


![](GroupProject2/Graph/occupation_vs_purchase.png)

## Box Plot: Consumer purchase habits across all occupation types

>   Occupation versus Purchase

>>  1.   On Black Friday everyone showed almost the same level of interest in purchasing and spent around the same amount of money.
>>  2.  Among all the occupations, occupation types 12, 15 and 17 did most of the purchases, amounting to around USD$14,000 
        followed by occupation types 7, 8 and 13 whose purchases amounted to almost USD$13,000.

![](GroupProject2/Graph/occupation_vs_purchase_box_plot.png)


### Box Plot: Consumer purchase behaviour across various categories

>   Age versus purchase:
>>  Almost all age groups took advantage of the lower prices and spent around the same amount of money.
    
>   Gender versus purchase:
>>  Men showed a little bit more interest than women in spending more money on Black Friday. 
    This might have been because of the huge discounts on electronic appliances and devices.   
    
>   Marital status versus purchase:
>>  Even though single people bought more products on Black Friday than married people, there was not a big diﬀerence in the purchase amount.
    0 - Single
    1 - Married
    
>   City versus purchase:
>>  People from city C showed a little bit more interest in shopping on Black Friday compared to people from cities A and B.


![](GroupProject2/Graph/consumer_behaviour_box.png)


### BAR Plot: Consumer purchase behaviour across various categories

>   Age versus purchase:
>>  Almost all age groups took advantage of the lower prices and spent around the same amount of money.
    
>   Gender versus purchase:
>>  Men showed a little bit more interest than women in spending more money on Black Friday. 
    This might have been because of the huge discounts on electronic appliances and devices.   
    
>   Marital status versus purchase:
>>  Even though single people bought more products on Black Friday than married people, there was not a big diﬀerence in the purchase amount.
    0 - Single
    1 - Married
    
>   City versus purchase:
>>  People from city C showed a little bit more interest in shopping on Black Friday compared to people from cities A and B.


![](GroupProject2/Graph/consumer_behaviour1.png)

  
###  Point Plots: Consumer purchase behaviour based on their age and city they live in

>    Important insights from the below point plots:

>>    1. The age group 55 and above was mostly interested in buying products from category-1, follow by products from category-2 and category-3.
>>    2. The age group between 0 and 17 were not quite active in purchasing. Most of them preferred buying products from category-3.
>>    3. The age group 55 and above made most of the purchases from city tier B. It is probable that city B is more populated compared to the other cities.
>>    4. Consumers of all age groups from city tier C did most of the purchases.

![](GroupProject2/Graph/consumer_behaviour_line.png)

###  Pie Plot: Consumer average purchase behaviour based on their age categories
> Consumer average purchase behaviour based on their age categories

![](GroupProject2/Graph/Average_Purchases_made_by_Age_group.png)


### Create Virtual Enviroment 

    1.   Navigate to your site's directory, where you'll create the new virtual environment
            cd ~/example.com
        
    2.   Create virtual environment
            conda create -n gProject2 python=3.7
        
    3.   Activate virtual environment
            conda activate gProject2
        
    4.   Install pipenv
            pip install pipenv
        
    5.   Install packages
            pipenv install numpy
            pipenv install bokeh
            pipenv install holoviews
            pipenv install pandas
            pipenv install numpy
            pipenv install sklearn
            pipenv install seaborn
         

## Steps/Options to run the solution:

    ### Option - 1
    
    1.   Git clone https://gitlab.com/cmsc6950/team2/data_analysis.git
    2.   Call run_init.py to download the dataset from Kaggle, clean data and plot graphs.
    
    ### Option - 2
    
    1.   Git clone https://gitlab.com/cmsc6950/team2/data_analysis.git
    2.   Call download.py file to download the dataset from Kaggle.
    2.   call load_dataset.py to load file
    3.   Call data_insight.py to plot graphs.
    4.   call bokeh_plot.py to plot bokeh plots
    
    ### Option - 3 (makefile)
    
    1.   Git clone https://gitlab.com/cmsc6950/team2/data_analysis.git 
        
    2.   Create virtual environment
            conda create -n gProject2 python=3.7
        
    3.   Activate virtual environment
            conda activate gProject2
    
    4.   Call makefile
            make


## Solutions:

### To run programs python 3 is needed. List of necessary packages are:

*   pip install virtualenv
*   pip install json
*   pip intsall zipfile
*   pip intsall matplotlib
*   pip intsall numpy
*   pip intsall pandas
*   pip intsall mpl_toolkits
*   pip intsall seaborn
*   pip intsall sklearn
*   pip intsall download
*   pip install bokeh
*   pip install holoviews


### Solution to download Black Friday dataset from Kaggle ###

 
    import os
    import json
    from zipfile import ZipFile
    
    # Method to create token file
    def create_token_file(kaggle_token_file):
        if not os.path.exists(kaggle_token_file):
            token = {"username":"kaushlenderk","key":"39d38a43aaf0fb250f887e943aa11e17"}
            json_token = json.dumps(token)
            f = open(kaggle_token_file,"w+")
            f.write(json_token)
            f.close()
            print("\nSuccessfully created kaggle token file........")
    
    # Method to unzip folder
    def unzip_folder(folder_path):
        with ZipFile(folder_path, 'r') as zipObj:
            zipObj.extractall("dataset/")
    
        print("\nZip folder extraction done........\n")
    
    # Method to call sub methods
    def download():
        try:
            # code to install kaggle
            try:
                import kaggle
                print("\nkaggle package all ready installed........\n")
            except ImportError as e:
                os.system("pip install kaggle")
                print("\nkaggle package installation is done........\n")
    
    
            # create directory for token file
            dirName=".kaggle"
            if not os.path.exists(dirName):
                os.mkdir(dirName)
                print("\nDirectory {} created successfully........\n".format(dirName))
    
            # Create kaggle token json file
            kaggle_token_file = ".kaggle/kaggle.json"
            create_token_file(kaggle_token_file)
    
            # list the black-friday folder structure
            os.system("kaggle datasets list -s black-friday")
    
            # download the black-friday zip folder
            if not os.path.exists("dataset/black-friday.zip"):
                os.system("kaggle datasets download -d mehdidag/black-friday -p dataset/")
    
            # Extract all the contents of black-friday.zip folder
            unzip_folder('dataset/black-friday.zip')
    
        except Exception as e:
            print(e)
    
    if __name__ == "__main__":
    
        # call file download process
        download()


### Solution to load clean Black Friday dataset ###

    import pandas as pd
    
    # Load black friday data set
    def load_data(file_name):
        nRowsRead = 1000
        blackfriday_data = pd.read_csv(file_name)
        return blackfriday_data
    
    # remove missing value or replace value NaN value with 0
    def clean_dataset(blackfriday_data,file_name):
    
        # Replace NaN value with 0
        blackfriday_data = blackfriday_data.fillna(0)
    
        # drop below columns
        cols = ['User_ID','Product_ID']
        blackfriday_data.drop(cols, inplace = True, axis =1)
    
        # remove + from data points
        blackfriday_data['Age']=(blackfriday_data['Age'].str.strip('+'))
        blackfriday_data['Stay_In_Current_City_Years']=(blackfriday_data['Stay_In_Current_City_Years'].str.strip('+').astype('float'))
    
        # write dataframe to files
        blackfriday_data.to_csv(file_name, index=False)
    
        blackfriday_data.Product_Category_2 = blackfriday_data.Product_Category_2.astype(int)
        blackfriday_data.Product_Category_3 = blackfriday_data.Product_Category_3.astype(int)
    
        return blackfriday_data


### Solution to plot matplotlib and seaborn plots ###

    import matplotlib.pyplot as plt
    import numpy as np
    import pandas as pd
    from mpl_toolkits.mplot3d import Axes3D
    import seaborn as sns
    from sklearn.preprocessing import Imputer
    import download as dw
    import load_dataset
    
    # plot columns data
    def plot_column_distribution(data_frame, ngraph_shown, ngraph_perrow):
        nunique = data_frame.nunique()
        data_frame = data_frame[[col for col in data_frame if nunique[col] > 1 and nunique[col] < 50]]
        nRow, nCol = data_frame.shape
        column_names = list(data_frame)
        ngraph_row = (nCol + ngraph_perrow - 1) / ngraph_perrow
        plt.figure(num = None, figsize = (6 * ngraph_perrow, 8 * ngraph_row), dpi = 300, facecolor = 'w', edgecolor = 'k')
        for i in range(min(nCol, ngraph_shown)):
            plt.subplot(ngraph_row, ngraph_perrow, i + 1)
            columnDf = data_frame.iloc[:, i]
            if (not np.issubdtype(type(columnDf.iloc[0]), np.number)):
                value_counts = columnDf.value_counts()
                value_counts.plot.bar()
            else:
                columnDf.hist()
            plt.ylabel('counts')
            plt.xticks(rotation = 90)
            plt.title(f'{column_names[i]}')
        plt.tight_layout(pad = 5.0, w_pad = 5.0, h_pad = 6.0)
        plt.show()
    
    
    # plot correlation matrix
    def plot_correlation_matrix(blackfriday_data):
        plt.figure(figsize=(12,5))
        sns.heatmap(
            blackfriday_data.corr(),
            annot=True,
            linewidth = 0.5
        )
    
    
    # Consumers purchase behaviour
    def purchase_occupation_behaviour(blackfriday_data):
        sns.jointplot(x='Occupation',y='Purchase',
                  data=blackfriday_data, kind='hex', color="b",height=8
                 )
    
        fig = plt.figure(figsize=(12,6))
        ax = fig.add_subplot(111)
    
        sns.boxplot(x = 'Occupation', y = 'Purchase', data = blackfriday_data, ax=ax)
        ax.set_title("Boxplot: purchase amount by occupations")
        plt.show()
    
    # Consumer behaviour based on Age, Gender, Marital_Status,City_Category
    def consumer_behaviour(blackfriday_data):
        fig, axes = plt.subplots(nrows=2, ncols=2,figsize=(18,14))
    
        blackfriday_data['Age_Encoded'] = blackfriday_data['Age'].map({'0-17':0,'18-25':1,
                              '26-35':2,'36-45':3,
                              '46-50':4,'51-55':5,
                              '55':6})
    
        ax = sns.boxplot(y='Purchase', x=blackfriday_data['Age'].sort_values(),data=blackfriday_data, ax=axes[0,0])
        ax = sns.boxplot(y='Purchase', x='Gender',data=blackfriday_data, ax=axes[0,1])
        ax = sns.boxplot(y='Purchase', x='Marital_Status',data=blackfriday_data,ax=axes[1,0])
        ax = sns.boxplot(y='Purchase', x='City_Category',data=blackfriday_data,ax=axes[1,1])
    
    # Consumer behaviour based on Age, city, Purchase
    def consumer_behaviour_type1(blackfriday_data):
        fig, axes = plt.subplots(nrows=2, ncols=2,figsize=(18,14))
    
        ax = sns.pointplot(y='Product_Category_1', x='City_Category', hue='Age', data=blackfriday_data, ax=axes[0,0])
        ax = sns.pointplot(y='Product_Category_2', x='City_Category', hue='Age', data=blackfriday_data, ax=axes[0,1])
        ax = sns.pointplot(y='Product_Category_3', x='City_Category', hue='Age', data=blackfriday_data, ax=axes[1,0])
        ax = sns.pointplot(y='Purchase', x='City_Category', hue='Age',data=blackfriday_data, ax=axes[1,1])
    
    
    # Average Purchases made by Age group
    def plot_average_purchase_by_age(blackfriday_data):
    
        dfData = pd.DataFrame()
        dfData = blackfriday_data[['Age','Purchase']]
    
        # Throw error in case of empty black firday data set
        assert dfData['Purchase'].count() != 0,"Assert error Black Friday Dataset is is empty."
    
        # calculate average purchase by age
        dfData['Purchase'] = dfData['Purchase'].astype(float)
        df_average = dfData.groupby(dfData['Age']).mean()
    
        df_average["Purchase"].plot.pie(autopct='%1.1f%%',counterclock=False, shadow=True,figsize=(9, 9))
        plt.title('Average Purchases made by Age group')
        plt.axes().set_ylabel('')
        plt.axes().set_xlabel('')
        plt.show()
    
    def call_data_insight_method(blackfriday_data):
    
        # Plot to get initial insight of the data points
        plot_column_distribution(blackfriday_data, 15, 4)
    
        # Plot correlation matrix of black friday dataset
        plot_correlation_matrix(blackfriday_data)
    
        # Consumer purchase behaviour
        # joint plot
        purchase_occupation_behaviour(blackfriday_data)
    
        # Consumer behaviour based on Age, Gender, Marital_Status,City_Category
        consumer_behaviour(blackfriday_data)
    
        # Consumer behaviour based on Age, city, Purchase
        consumer_behaviour_type1(blackfriday_data)
    
        # Average Purchases made by Age group
        plot_average_purchase_by_age(blackfriday_data)


### Solution to plot Bokeh plots ###

    import pandas as pd
    from math import pi
    from bokeh.io import output_file, save,show
    from bokeh.palettes import  viridis, Category10
    from bokeh.plotting import figure
    from bokeh.transform import cumsum
    import holoviews as hv
    from holoviews import opts, Palette
    from bokeh.palettes import Category20c
    from bokeh.models import HoverTool
    import numpy as np
    import load_dataset
    
    
    def map_age_field(age):
        if age == '0-17':
            return 0
        elif age == '18-25':
            return 1
        elif age == '26-35':
            return 2
        elif age == '36-45':
            return 3
        elif age == '46-50':
            return 4
        elif age == '51-55':
            return 5
        else:
            return 6
    
    
    def plot_bar_age_vs_purchase(blackfriday_data):
    
        dfData = pd.DataFrame()
        dfData=blackfriday_data[['Age','Purchase']]
        dfData['Age'] = dfData['Age'].apply(map_age_field)
    
        # calculate average purchase by age
        dfData['Purchase'] = dfData['Purchase'].astype(float)
        df_average = dfData.groupby(dfData['Age']).count().reset_index()
    
        TOOLS = "hover,save,pan,box_zoom,reset,wheel_zoom,tap"
        p = figure(plot_height=350, title="Purchase amount by Age",tools=TOOLS,toolbar_location='above')
    
        p.vbar(x=df_average.Age, top=df_average.Purchase, width=0.9)
    
        p.y_range.start = 0
        p.x_range.range_padding = 0.1
        p.xgrid.grid_line_color = None
        p.axis.minor_tick_line_color = None
        p.outline_line_color = None
        p.xaxis.axis_label = 'Age'
        p.yaxis.axis_label = 'Purchase'
        p.select_one(HoverTool).tooltips = [
            ('Age', '@x'),
            ('Purchase', '@top'),
        ]
        output_file("BokehPlot/bar_age_vs_purchase.html", title="barchart")
        show(p)
    
    def map_gender_field(gender):
        if gender == 'F':
            return 1
        else:
            return 0
    
    def plot_bar_gender_vs_purchase(blackfriday_data):
        dfData = pd.DataFrame()
        dfData=blackfriday_data[['Gender','Purchase']]
        dfData['Gender'] = dfData['Gender'].apply(map_gender_field)
    
    
        # calculate average purchase by age
        dfData['Purchase'] = dfData['Purchase'].astype(float)
        df_average = dfData.groupby(dfData['Gender']).count().reset_index()
    
    
        TOOLS = "hover,save,pan,box_zoom,reset,wheel_zoom,tap"
        p = figure(plot_height=350, title="Purchase amount by Gender",tools=TOOLS,toolbar_location='above')
    
        p.vbar(x=df_average.Gender, top=df_average.Purchase, width=0.9)
    
        p.y_range.start = 0
        p.x_range.range_padding = 0.1
        p.xgrid.grid_line_color = None
        p.axis.minor_tick_line_color = None
        p.outline_line_color = None
        p.xaxis.axis_label = 'Gender'
        p.yaxis.axis_label = 'Purchase'
        p.select_one(HoverTool).tooltips = [
            ('Gender', '@x'),
            ('Purchase', '@top'),
        ]
        output_file("BokehPlot/bar_Gender_vs_purchase.html", title="barchart")
        show(p)
    
    def plot_bar_maritalstatus_vs_purchase(blackfriday_data):
        dfData = pd.DataFrame()
        dfData=blackfriday_data[['Marital_Status','Purchase']]
    
        # calculate average purchase by age
        dfData['Purchase'] = dfData['Purchase'].astype(float)
        df_average = dfData.groupby(dfData['Marital_Status']).count().reset_index()
    
    
        TOOLS = "hover,save,pan,box_zoom,reset,wheel_zoom,tap"
        p = figure(plot_height=350, title="Purchase amount by Marital_Status",tools=TOOLS,toolbar_location='above')
    
        p.vbar(x=df_average.Marital_Status, top=df_average.Purchase, width=0.9)
    
        p.y_range.start = 0
        p.x_range.range_padding = 0.1
        p.xgrid.grid_line_color = None
        p.axis.minor_tick_line_color = None
        p.outline_line_color = None
        p.xaxis.axis_label = 'Marital Status'
        p.yaxis.axis_label = 'Purchase'
        p.select_one(HoverTool).tooltips = [
            ('Marital Status', '@x'),
            ('Purchase', '@top'),
        ]
        output_file("BokehPlot/bar_Marital_Status_vs_purchase.html", title="barchart")
        show(p)
    
    def map_city_categories_field(city_category):
        if city_category == 'A':
            return 2
        elif city_category == 'B':
            return 1
        else:
            return 0
    
    
    def plot_bar_city_category_vs_purchase(blackfriday_data):
        dfData = pd.DataFrame()
        dfData=blackfriday_data[['City_Category','Purchase']]
        dfData['City_Category'] = dfData['City_Category'].apply(map_city_categories_field)
    
        # calculate average purchase by age
        dfData['Purchase'] = dfData['Purchase'].astype(float)
        df_average = dfData.groupby(dfData['City_Category']).count().reset_index()
    
    
        TOOLS = "hover,save,pan,box_zoom,reset,wheel_zoom,tap"
        p = figure(plot_height=350, title="Purchase amount by City Category",tools=TOOLS,toolbar_location='above')
    
        p.vbar(x=df_average.City_Category, top=df_average.Purchase, width=0.9)
    
        p.y_range.start = 0
        p.x_range.range_padding = 0.1
        p.xgrid.grid_line_color = None
        p.axis.minor_tick_line_color = None
        p.outline_line_color = None
        p.xaxis.axis_label = 'City Category'
        p.yaxis.axis_label = 'Purchase'
        p.select_one(HoverTool).tooltips = [
            ('City Category', '@x'),
            ('Purchase', '@top'),
        ]
        output_file("BokehPlot/bar_City_Category_vs_purchase.html", title="barchart")
        show(p)
    
    def plot_average_purchase_by_age(blackfriday_data):
    
        output_file("pie.html")
    
        dfData = pd.DataFrame()
        dfData=blackfriday_data[['Age','Purchase']]
    
        # calculate average purchase by age
        dfData['Purchase'] = dfData['Purchase'].astype(float)
        df_average = dfData.groupby(dfData['Age']).mean().reset_index()
    
        df_average['angle'] = df_average['Purchase']/df_average['Purchase'].sum() * 2*pi
    
        df_average['color'] = Category20c[len(df_average['Purchase'])]
    
        p = figure(plot_height=350, title="Average purchases made by Age groups", toolbar_location=None,
                   tools="hover", tooltips="@Age: @Purchase", x_range=(-0.5, 1.0))
    
        p.wedge(x=0, y=1, radius=0.4,  start_angle=cumsum('angle', include_zero=True), end_angle=cumsum('angle'),
                line_color="white", fill_color='color', legend='Age', source=df_average)
    
    
        p.axis.axis_label=None
        p.axis.visible=False
        p.grid.grid_line_color = None
    
        output_file("BokehPlot/pie.html", title="barchart")
        show(p)
    
    
    # main method to call sub plot method
    def call_bokeh_plot(blackfriday_data):
    
        plot_bar_age_vs_purchase(blackfriday_data)
    
        plot_bar_gender_vs_purchase(blackfriday_data)
    
        plot_bar_maritalstatus_vs_purchase(blackfriday_data)
    
        plot_bar_city_category_vs_purchase(blackfriday_data)
    
        plot_average_purchase_by_age(blackfriday_data)
    
    
    if __name__ == "__main__":
    
        # Load dataset
        new_file_name = "dataset/Clean_BlackFriday.csv"
    
        blackfriday_data = load_dataset.load_data(new_file_name)
        blackfriday_data.dataframeName = 'Black Friday dataset'
    
        call_bokeh_plot(blackfriday_data)



### Main solution file to call sub packages ###

    import download as dw
    import data_insight as dInsight
    import bokeh_plot as bp
    import load_dataset
    
    
    if __name__ == "__main__":
    
        # Call download process
        dw.download()
    
        # Call method to load black friday data from files
        file_name = "dataset/BlackFriday.csv"
        blackfriday_dataframe = load_dataset.load_data(file_name)
    
        # Clean dataset remove/replace missing value from dataset
        new_file_name = "dataset/Clean_BlackFriday.csv"
        blackfriday_data = load_dataset.clean_dataset(blackfriday_dataframe,new_file_name)
        blackfriday_data.dataframeName = 'Black Friday dataset'
    
        # Plot to get initial insight of the data points
        dInsight.call_data_insight_method(blackfriday_data)
    
        # bokeh plot
        bp.call_bokeh_plot(blackfriday_data)
        
        
### Test assert file to check if the dataset is empty ###
    
    import load_dataset
    import bokeh_plot as bp
    import pytest
    import pandas as pd
    
    def test_empty_dataset():
    
        new_file_name = "dataset/Clean_BlackFriday.csv"
        blackfriday_dataframe = load_dataset.load_data(new_file_name)
    
        assert blackfriday_dataframe['Purchase'].count() != 0,"Assert error to check if Black Friday Dataset is empty."
    
    def test_empty_age_field_plot():
    
        new_file_name = "dataset/Clean_BlackFriday.csv"
        blackfriday_dataframe = load_dataset.load_data(new_file_name)
    
        dfData = pd.DataFrame()
        dfData=blackfriday_dataframe[['Age','Purchase']]
        dfData['Age'] = dfData['Age'].apply(bp.map_age_field)
    
        assert dfData['Age'].count() != 0,"Assert error to check if empty Age field column."
    
    def test_empty_gender_field_plot():
    
        new_file_name = "dataset/Clean_BlackFriday.csv"
        blackfriday_dataframe = load_dataset.load_data(new_file_name)
    
        dfData = pd.DataFrame()
        dfData=blackfriday_dataframe[['Gender','Purchase']]
        dfData['Gender'] = dfData['Gender'].apply(bp.map_gender_field)
    
        assert dfData['Gender'].count() != 0,"Assert error to check if empty Gender field column."
    
    def test_empty_city_field_plot():
    
        new_file_name = "dataset/Clean_BlackFriday.csv"
        blackfriday_dataframe = load_dataset.load_data(new_file_name)
    
        dfData = pd.DataFrame()
        dfData=blackfriday_dataframe[['City_Category','Purchase']]
        dfData['City_Category'] = dfData['City_Category'].apply(bp.map_city_categories_field)
    
        assert dfData['City_Category'].count() != 0,"Assert error to check if empty City field column."



## References

> [Black Friday — A study of sales trough consumer behaviours](https://www.kaggle.com/mehdidag/black-friday#BlackFriday.csv)

> [Medium — Diogo Menezes Borges ](https://medium.com/diogo-menezes-borges/project-3-analytics-vidhya-hackaton-black-friday-f6c6bf3da86f)